#!/bin/sh

#################
# Pretty output #
#################

notice()  { printf "\n\033[0;33m [?] \033[0m$*\n"; step_msg=$*; }
info()    { printf      "\033[0m [!] \033[0m$*\n";              }
fail()    { printf   "\033[0;31m [x] \033[0m$*\n\n"; exit 1;    }
success() { printf   "\033[0;32m [*] \033[0m${step_msg}\n\n";   }

############################
# A few important paths... #
############################

export out=$PWD/out
export res=$PWD/build-resources
steps=$PWD/build-steps

# Check if running as root
notice "Checking if root"
if [ $(whoami) != "root" ]; then
    info "Running as $(whoami)"
    fail "This script must be run as root"
    exit 1
fi
success

# Assemble the filesystem
rm $out -rf
$steps/base-fs && \
$steps/akoziland-fs-essentials && \
$steps/build-modules

# Make a squashfs from the created filesystem

notice "Creating squashfs"
mksquashfs out/fs out/root.squashfs && \
success || \
fail "Squashfs was not successfully created"
